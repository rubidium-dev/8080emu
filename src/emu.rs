use crate::state::State;

macro_rules! set_flag_if {
    ( $op:expr, $val:expr ) => {
        match $op {
            $val => 1,
            _ => 0,
        }
    };
}

macro_rules! clear_flag_if {
    ( $op:expr, $val:expr ) => {
        match $op {
            $val => 0,
            _ => 1,
        }
    };
}

/// Calculates the parity bit for `value`. Result will be 1 if there are an even
/// number of bits, otherwise 0.
fn parity(value: u8) -> u8 {
    // this is the expensive way... couple of ideas if this becomes a problem:
    // 1) use a lookup table, 2) late-calculate the parity bit when it's actually used
    const SIZE: u16 = 8;
    let mut p: u16 = 0;
    let mut value: u16 = (value as u16) & ((1 << SIZE) - 1);
    for _ in 0..SIZE {
        if (value & 0x01) != 0 {
            p += 1;
        }
        value = value >> 1;
    }
    if (p & 0x01) == 0 {
        1
    } else {
        0
    }
}

/// Sets the CPU flags based on a given `value`. Does not alter carry flag.
fn set_flags(value: u8, mut state: &mut State) {
    // set zero flag if value is zero
    state.cc.z = set_flag_if!(value, 0);
    // set sign flag if bit 7 is set
    state.cc.s = clear_flag_if!(value & 0x80, 0);
    // calculate parity bit
    state.cc.p = parity(value);
}

/// Sets the CPU flags based on a given `value`. Clears carry flag.
fn set_flags_clear_carry(value: u8, mut state: &mut State) {
    set_flags(value, &mut state);
    state.cc.cy = 0;
}

/// Adds `value` to the accumulator register (A) and sets appropriate flags.
fn add_to_a(value: u8, mut state: &mut State) {
    add_to_a_with_cy(value, 0, &mut state);
}

/// Adds `value` to the accumulator register (A) with optional carry (CY)
/// and sets appropriate flags.
fn add_to_a_with_cy(value: u8, carry: u8, mut state: &mut State) {
    // do the math with higher precision to capture carry
    let answer = (state.a as u16)
        .wrapping_add(value as u16)
        .wrapping_add(carry as u16);
    // set zero flag if result is zero
    state.cc.z = set_flag_if!(answer & 0xff, 0);
    // set sign flag if bit 7 is set
    state.cc.s = clear_flag_if!(answer & 0x80, 0);
    // set carry flag if overflow
    state.cc.cy = set_flag_if!(answer > 0xff, true);
    // place result in the accumulator
    state.a = (answer & 0xff) as u8;
    // calculate parity bit
    state.cc.p = parity(state.a);
}

/// Subtracts `value from the accumulator register (A) and sets appropriate flags.
fn sub_from_a(value: u8, mut state: &mut State) {
    sub_from_a_with_cy(value, 0, &mut state);
}

/// Subtracts `value` from the accumulator register (A) with optional carry (CY)
/// and sets appropriate flags.
fn sub_from_a_with_cy(value: u8, carry: u8, mut state: &mut State) {
    let answer = (state.a as u16)
        .wrapping_sub(value as u16)
        .wrapping_sub(carry as u16);
    state.cc.z = set_flag_if!(answer & 0xff, 0);
    state.cc.s = clear_flag_if!(answer & 0x80, 0);
    state.cc.cy = set_flag_if!(state.a < value, true);
    state.a = (answer & 0xff) as u8;
    state.cc.p = parity(state.a);
}

/// Adds `value` to HL, only sets carry flag.
fn add_to_hl(value: u16, mut state: &mut State) {
    let answer = (state.hl() as u32).wrapping_add(value as u32);
    state.set_hl(u16_to_bytes!((answer & 0xffff) as u16));
    state.cc.cy = clear_flag_if!(answer & 0xffff0000, 0);
}

/// Compares `value` with the accumulator register (A), sets flags as appropriate.
/// Z flag will be set if equal, carry flag (CY) is set if `value` is greater than A.
fn compare(value: u8, mut state: &mut State) {
    // use subtraction to generate comparison
    set_flags(state.a.wrapping_sub(value), &mut state);
    if state.a < value {
        state.cc.cy = 1;
    } else {
        state.cc.cy = 0;
    }
}

/// DAA (Decimal Adjust Accumulator) implementation. Sets appropriate flags,
/// including the AC flag.
fn daa(mut state: &mut State) {
    // The eight-bit hexadecimal number in the accumulator is adjusted to form
    // two four-bit binary-encoded decimal digits by the following two-step
    // process:
    // 1. If the 4x LSB of A represents a number greater than 9 or the AC
    //    flag is set, the accumulator is incremented by 6.
    // 2. If the 4x MSB of A now represent a number greater than 9 or the
    //    CY flag is set, the 4x MSB of the accumulator are incremenented
    //    by 6.
    // If a carry out of the 4x LSB occurs during step 1, the AC flag is set,
    // otherwise it is cleared. Likewise, if a carry out of the 4x MSB occurs
    // during step 2, the CY flag is set, otherwise it is not changed.
    // This instruction is used when adding decimal numbers. It is the only
    // instruction that uses the AC flag.
    let a_lsb = state.a & 0x0f;
    if a_lsb > 9 || state.cc.ac == 1 {
        state.a += 6;
        state.cc.ac = set_flag_if!((a_lsb + 6) > 0x0f, true);
    }
    let mut a_msb = state.a >> 4;
    if a_msb > 9 {
        a_msb += 6;
        if a_msb > 0x0f {
            state.cc.cy = 1;
        }
    }
    state.a = (a_msb << 4) | (state.a & 0x0f);
}

/// Reads opcode at current PC and attempts to execute it.
pub fn emulate_8080_op(mut mem: &mut [u8], mut state: &mut State) -> u32 {
    let pc = state.pc();
    let (pc_adj, cycles) = match mem[pc] {
        0x00 | 0x08 | 0x10 | 0x18 | 0x20 | 0x28 | 0x30 | 0x38 | 0xcb | 0xd9 | 0xdd | 0xed
        | 0xfd => (1, 4), // NOP
        0x01 => {
            // LXI B,word
            state.set_bc(&mem[pc + 1..=pc + 2]);
            (3, 10)
        }
        0x02 => {
            // STAX B (store A into the address BC)
            mem[state.bc() as usize] = state.a;
            (1, 7)
        }
        0x03 => {
            // INX B (increment BC without affecting flags)
            state.set_bc(u16_to_bytes!(state.bc().wrapping_add(1)));
            (1, 5)
        }
        0x04 => {
            // INR B (incrememnt B, doesn't affect carry flag)
            state.b = state.b.wrapping_add(1);
            set_flags(state.b, &mut state);
            (1, 5)
        }
        0x05 => {
            // DCR B (decrement B, doesn't affect carry flag)
            state.b = state.b.wrapping_sub(1);
            set_flags(state.b, &mut state);
            (1, 5)
        }
        0x06 => {
            // MVI B,byte
            state.b = mem[pc + 1];
            (2, 7)
        }
        0x07 => {
            // RLC (rotate A left, only affects carry flag)
            let x = state.a;
            // if the leftmost bit was 1, this will put it into the rightmost bit after rotating x
            state.a = ((x & 0x80) >> 7) | (x << 1);
            // carry flag is set if the original leftmost bit was 1
            state.cc.cy = set_flag_if!(x & 0x80, 0x80);
            (1, 4)
        }
        0x09 => {
            // DAD B (adds BC to HL, only affects carry flag)
            add_to_hl(state.bc(), &mut state);
            (1, 10)
        }
        0x0a => {
            // LDAX B (store value at address BC into A)
            state.a = mem[state.bc() as usize];
            (1, 7)
        }
        0x0b => {
            // DCX B (decrement BC without affecting flags)
            state.set_bc(u16_to_bytes!(state.bc().wrapping_sub(1)));
            (1, 5)
        }
        0x0c => {
            // INR C (increment C, doesn't affect carry flag)
            state.c = state.c.wrapping_add(1);
            set_flags(state.c, &mut state);
            (1, 5)
        }
        0x0d => {
            // DCR C (decrement C, doesn't affect carry flag)
            state.c = state.c.wrapping_sub(1);
            set_flags(state.c, &mut state);
            (1, 5)
        }
        0x0e => {
            // MVI C,byte
            state.c = mem[pc + 1];
            (2, 7)
        }
        0x0f => {
            // RRC (rotate A right, only affects carry flag)
            let x = state.a;
            // if the rightmost bit was 1, this will put it into the leftmost bit after rotating x
            state.a = ((x & 1) << 7) | (x >> 1);
            // carry flag is set if the original rightmost bit was 1
            state.cc.cy = set_flag_if!(x & 1, 1);
            (1, 4)
        }
        0x11 => {
            // LXI D,word
            state.set_de(&mem[pc + 1..=pc + 2]);
            (3, 10)
        }
        0x12 => {
            // STAX D (store A into the address DE)
            mem[state.de() as usize] = state.a;
            (1, 7)
        }
        0x13 => {
            // INX D (increment DE without affecting flags)
            state.set_de(u16_to_bytes!(state.de().wrapping_add(1)));
            (1, 5)
        }
        0x14 => {
            // INR D (increment D, doesn't affect carry flag)
            state.d = state.d.wrapping_add(1);
            set_flags(state.d, &mut state);
            (1, 5)
        }
        0x15 => {
            // DCR D (decrement D, doesn't affect carry flag)
            state.d = state.d.wrapping_sub(1);
            set_flags(state.d, &mut state);
            (1, 5)
        }
        0x16 => {
            // MVI D,byte
            state.d = mem[pc + 1];
            (2, 7)
        }
        0x17 => {
            // RAL (rotate A left, uses the carry flag to set the rightmost bit)
            let x = state.a;
            // if the carry flag is set, this will set the rightmost bit to 1 after rotating x
            state.a = state.cc.cy | (x << 1);
            // carry flag is set if the original leftmost bit was 1
            state.cc.cy = set_flag_if!(x & 0x80, 0x80);
            (1, 4)
        }
        0x19 => {
            // DAD D (adds DE to HL, only affects carry flag)
            add_to_hl(state.de(), &mut state);
            (1, 10)
        }
        0x1a => {
            // LDAX D (store value at address DE into A)
            state.a = mem[state.de() as usize];
            (1, 7)
        }
        0x1b => {
            // DCX D (decrement DE without affecting flags)
            state.set_de(u16_to_bytes!(state.de().wrapping_sub(1)));
            (1, 5)
        }
        0x1c => {
            // INR E (increment E, doesn't affect carry flag)
            state.e = state.e.wrapping_add(1);
            set_flags(state.e, &mut state);
            (1, 5)
        }
        0x1d => {
            // DCR E (decrement E, doesn't affect carry flag)
            state.e = state.e.wrapping_sub(1);
            set_flags(state.e, &mut state);
            (1, 5)
        }
        0x1e => {
            // MVI E,byte
            state.e = mem[pc + 1];
            (2, 7)
        }
        0x1f => {
            // RAR (rotate A right, uses the carry flag to set the leftmost bit)
            let x = state.a;
            // if the carry flag is set, this will set the leftmost bit to 1 after rotating x
            state.a = (state.cc.cy << 7) | (x >> 1);
            // carry flag is set if the original rightmost bit was 1
            state.cc.cy = set_flag_if!(x & 1, 1);
            (1, 4)
        }
        0x21 => {
            // LXI H,word
            state.set_hl(&mem[pc + 1..=pc + 2]);
            (3, 10)
        }
        0x22 => {
            // SHLD addr
            let addr = bytes_to_u16!(mem[pc + 2], mem[pc + 1]) as usize;
            mem[addr] = state.l;
            mem[addr + 1] = state.h;
            (3, 16)
        }
        0x23 => {
            // INX H (increment HL without affecting flags)
            state.set_hl(u16_to_bytes!(state.hl().wrapping_add(1)));
            (1, 5)
        }
        0x24 => {
            // INR H (increment H, doesn't affect carry flag)
            state.h = state.h.wrapping_add(1);
            set_flags(state.h, &mut state);
            (1, 5)
        }
        0x25 => {
            // DCR H (decrement H, doesn't affect carry flag)
            state.h = state.h.wrapping_sub(1);
            set_flags(state.h, &mut state);
            (1, 5)
        }
        0x26 => {
            // MVI H,byte
            state.h = mem[pc + 1];
            (2, 7)
        }
        0x27 => {
            // DAA (decimal adjust accumulator)
            daa(&mut state);
            (1, 4)
        }
        0x29 => {
            // DAD H (adds HL to HL, only affects carry flag)
            add_to_hl(state.hl(), &mut state);
            (1, 10)
        }
        0x2a => {
            // LHLD word
            let addr = bytes_to_u16!(mem[pc + 2], mem[pc + 1]) as usize;
            state.l = mem[addr];
            state.h = mem[addr + 1];
            (3, 16)
        }
        0x2b => {
            // DCX H (decrement HL without affecting flags)
            state.set_hl(u16_to_bytes!(state.hl().wrapping_sub(1)));
            (1, 5)
        }
        0x2c => {
            // INR L (increment L, doesn't affect carry flag)
            state.l = state.l.wrapping_add(1);
            set_flags(state.l, &mut state);
            (1, 5)
        }
        0x2d => {
            // DCR L (decrement L, doesn't affect carry flag)
            state.l = state.l.wrapping_sub(1);
            set_flags(state.l, &mut state);
            (1, 5)
        }
        0x2e => {
            // MVI L,byte
            state.l = mem[pc + 1];
            (2, 7)
        }
        0x2f => {
            // CMA (not)
            // does not affect flags
            state.a = !state.a;
            (1, 4)
        }
        0x31 => {
            // LXI SP,word
            state.set_sp(&mem[pc + 1..=pc + 2]);
            (3, 10)
        }
        0x32 => {
            // STA addr
            let addr = bytes_to_u16!(mem[pc + 2], mem[pc + 1]) as usize;
            mem[addr] = state.a;
            (3, 13)
        }
        0x33 => {
            // INX SP (increment SP without affecting flags)
            state.sp = state.sp.wrapping_add(1);
            (1, 5)
        }
        0x34 => {
            // INR M (increment value at HL, doesn't affect carry flag)
            let value = mem[state.at_hl()].wrapping_add(1);
            mem[state.at_hl()] = value;
            set_flags(value, &mut state);
            (1, 10)
        }
        0x35 => {
            // DCR M (decrement value at HL, doesn't affect carry flag)
            let value = mem[state.at_hl()].wrapping_sub(1);
            mem[state.at_hl()] = value;
            set_flags(value, &mut state);
            (1, 10)
        }
        0x36 => {
            // MVI M,byte
            mem[state.at_hl()] = mem[pc + 1];
            (2, 10)
        }
        0x37 => {
            // STC
            state.cc.cy = 1;
            (1, 4)
        }
        0x39 => {
            // DAD SP (adds SP to HL, only affects carry flag)
            add_to_hl(state.sp as u16, &mut state);
            (1, 10)
        }
        0x3a => {
            // LDA addr
            let addr = bytes_to_u16!(mem[pc + 2], mem[pc + 1]) as usize;
            state.a = mem[addr];
            (3, 13)
        }
        0x3b => {
            // DCX SP (decrement SP without affecting flags)
            state.sp = state.sp.wrapping_sub(1);
            (1, 5)
        }
        0x3c => {
            // INR A (increment A, doesn't affect carry flag)
            state.a = state.a.wrapping_add(1);
            set_flags(state.a, &mut state);
            (1, 5)
        }
        0x3d => {
            // DCR A (decrement A, doesn't affect carry flag)
            state.a = state.a.wrapping_sub(1);
            set_flags(state.a, &mut state);
            (1, 5)
        }
        0x3e => {
            // MVI A,byte
            state.a = mem[pc + 1];
            (2, 7)
        }
        0x3f => {
            // CMC
            state.cc.cy = !state.cc.cy;
            (1, 4)
        }
        0x40 => {
            // MOV B,B
            (1, 5)
        }
        0x41 => {
            // MOV B,C
            state.b = state.c;
            (1, 5)
        }
        0x42 => {
            // MOV B,D
            state.b = state.d;
            (1, 5)
        }
        0x43 => {
            // MOV B,E
            state.b = state.e;
            (1, 5)
        }
        0x44 => {
            // MOV B,H
            state.b = state.h;
            (1, 5)
        }
        0x45 => {
            // MOV B,L
            state.b = state.l;
            (1, 5)
        }
        0x46 => {
            // MOV B,M
            state.b = mem[state.at_hl()];
            (1, 7)
        }
        0x47 => {
            // MOV B,A
            state.b = state.a;
            (1, 5)
        }
        0x48 => {
            // MOV C,B
            state.c = state.b;
            (1, 5)
        }
        0x49 => {
            // MOV C,C
            (1, 5)
        }
        0x4a => {
            // MOV C,D
            state.c = state.d;
            (1, 5)
        }
        0x4b => {
            // MOV C,E
            state.c = state.e;
            (1, 5)
        }
        0x4c => {
            // MOV C,H
            state.c = state.h;
            (1, 5)
        }
        0x4d => {
            // MOV C,L
            state.c = state.l;
            (1, 5)
        }
        0x4e => {
            // MOV C,M
            state.c = mem[state.at_hl()];
            (1, 7)
        }
        0x4f => {
            // MOV C,A
            state.c = state.a;
            (1, 5)
        }
        0x50 => {
            // MOV D,B
            state.d = state.b;
            (1, 5)
        }
        0x51 => {
            // MOV D,C
            state.d = state.c;
            (1, 5)
        }
        0x52 => {
            // MOV D,D
            (1, 5)
        }
        0x53 => {
            // MOV D,E
            state.d = state.e;
            (1, 5)
        }
        0x54 => {
            // MOV D,H
            state.d = state.h;
            (1, 5)
        }
        0x55 => {
            // MOV D,L
            state.d = state.l;
            (1, 5)
        }
        0x56 => {
            // MOV D,M
            state.d = mem[state.at_hl()];
            (1, 7)
        }
        0x57 => {
            // MOV D,A
            state.d = state.a;
            (1, 5)
        }
        0x58 => {
            // MOV E,B
            state.e = state.b;
            (1, 5)
        }
        0x59 => {
            // MOV E,C
            state.e = state.c;
            (1, 5)
        }
        0x5a => {
            // MOV E,D
            state.e = state.d;
            (1, 5)
        }
        0x5b => {
            // MOV E,E
            (1, 5)
        }
        0x5c => {
            // MOV E,H
            state.e = state.h;
            (1, 5)
        }
        0x5d => {
            // MOV E,L
            state.e = state.l;
            (1, 5)
        }
        0x5e => {
            // MOV E,M
            state.e = mem[state.at_hl()];
            (1, 7)
        }
        0x5f => {
            // MOV E,A
            state.e = state.a;
            (1, 5)
        }
        0x60 => {
            // MOV H,B
            state.h = state.b;
            (1, 5)
        }
        0x61 => {
            // MOV H,C
            state.h = state.c;
            (1, 5)
        }
        0x62 => {
            // MOV H,D
            state.h = state.d;
            (1, 5)
        }
        0x63 => {
            // MOV H,E
            state.h = state.e;
            (1, 5)
        }
        0x64 => {
            // MOV H,H
            (1, 5)
        }
        0x65 => {
            // MOV H,L
            state.h = state.l;
            (1, 5)
        }
        0x66 => {
            // MOV H,M
            state.h = mem[state.at_hl()];
            (1, 7)
        }
        0x67 => {
            // MOV H,A
            state.h = state.a;
            (1, 5)
        }
        0x68 => {
            // MOV L,B
            state.l = state.b;
            (1, 5)
        }
        0x69 => {
            // MOV L,C
            state.l = state.c;
            (1, 5)
        }
        0x6a => {
            // MOV L,D
            state.l = state.d;
            (1, 5)
        }
        0x6b => {
            // MOV L,E
            state.l = state.e;
            (1, 5)
        }
        0x6c => {
            // MOV L,H
            state.l = state.h;
            (1, 5)
        }
        0x6d => {
            // MOV L,L
            (1, 5)
        }
        0x6e => {
            // MOV L,M
            state.l = mem[state.at_hl()];
            (1, 7)
        }
        0x6f => {
            // MOV L,A
            state.l = state.a;
            (1, 5)
        }
        0x70 => {
            // MOV M,B
            mem[state.at_hl()] = state.b;
            (1, 7)
        }
        0x71 => {
            // MOV M,C
            mem[state.at_hl()] = state.c;
            (1, 7)
        }
        0x72 => {
            // MOV M,D
            mem[state.at_hl()] = state.d;
            (1, 7)
        }
        0x73 => {
            // MOV M,E
            mem[state.at_hl()] = state.e;
            (1, 7)
        }
        0x74 => {
            // MOV M,H
            mem[state.at_hl()] = state.h;
            (1, 7)
        }
        0x75 => {
            // MOV M,L
            mem[state.at_hl()] = state.l;
            (1, 7)
        }
        0x76 => {
            // HLT
            panic!("Program halted.");
        }
        0x77 => {
            // MOV M,A
            mem[state.at_hl()] = state.a;
            (1, 7)
        }
        0x78 => {
            // MOV A,B
            state.a = state.b;
            (1, 5)
        }
        0x79 => {
            // MOV A,C
            state.a = state.c;
            (1, 5)
        }
        0x7a => {
            // MOV A,D
            state.a = state.d;
            (1, 5)
        }
        0x7b => {
            // MOV A,E
            state.a = state.e;
            (1, 5)
        }
        0x7c => {
            // MOV A,H
            state.a = state.h;
            (1, 5)
        }
        0x7d => {
            // MOV A,L
            state.a = state.l;
            (1, 5)
        }
        0x7e => {
            // MOV A,M
            state.a = mem[state.at_hl()];
            (1, 7)
        }
        0x7f => {
            // MOV A,A
            (1, 5)
        }
        0x80 => {
            // ADD B
            add_to_a(state.b, &mut state);
            (1, 4)
        }
        0x81 => {
            // ADD C
            add_to_a(state.c, &mut state);
            (1, 4)
        }
        0x82 => {
            // ADD D
            add_to_a(state.d, &mut state);
            (1, 4)
        }
        0x83 => {
            // ADD E
            add_to_a(state.e, &mut state);
            (1, 4)
        }
        0x84 => {
            // ADD H
            add_to_a(state.h, &mut state);
            (1, 4)
        }
        0x85 => {
            // ADD L
            add_to_a(state.l, &mut state);
            (1, 4)
        }
        0x86 => {
            // ADD M
            add_to_a(mem[state.at_hl()], &mut state);
            (1, 7)
        }
        0x87 => {
            // ADD A
            add_to_a(state.a, &mut state);
            (1, 4)
        }
        0x88 => {
            // In the carry variants (ADC, ACI, SBB, SUI) you use the carry bit in the calculation as indicated in the data book.
            // ADC B
            add_to_a_with_cy(state.b, state.cc.cy, &mut state);
            (1, 4)
        }
        0x89 => {
            // ADC C
            add_to_a_with_cy(state.c, state.cc.cy, &mut state);
            (1, 4)
        }
        0x8a => {
            // ADC D
            add_to_a_with_cy(state.d, state.cc.cy, &mut state);
            (1, 4)
        }
        0x8b => {
            // ADC E
            add_to_a_with_cy(state.e, state.cc.cy, &mut state);
            (1, 4)
        }
        0x8c => {
            // ADC H
            add_to_a_with_cy(state.h, state.cc.cy, &mut state);
            (1, 4)
        }
        0x8d => {
            // ADC L
            add_to_a_with_cy(state.l, state.cc.cy, &mut state);
            (1, 4)
        }
        0x8e => {
            // ADC M
            add_to_a_with_cy(mem[state.at_hl()], state.cc.cy, &mut state);
            (1, 7)
        }
        0x8f => {
            // ADC A
            add_to_a_with_cy(state.a, state.cc.cy, &mut state);
            (1, 4)
        }
        0x90 => {
            // SUB B
            sub_from_a(state.b, &mut state);
            (1, 4)
        }
        0x91 => {
            // SUB C
            sub_from_a(state.c, &mut state);
            (1, 4)
        }
        0x92 => {
            // SUB D
            sub_from_a(state.d, &mut state);
            (1, 4)
        }
        0x93 => {
            // SUB E
            sub_from_a(state.e, &mut state);
            (1, 4)
        }
        0x94 => {
            // SUB H
            sub_from_a(state.h, &mut state);
            (1, 4)
        }
        0x95 => {
            // SUB L
            sub_from_a(state.l, &mut state);
            (1, 4)
        }
        0x96 => {
            // SUB M
            sub_from_a(mem[state.at_hl()], &mut state);
            (1, 7)
        }
        0x97 => {
            // SUB A
            sub_from_a(state.a, &mut state);
            (1, 4)
        }
        0x98 => {
            // SBB B
            sub_from_a_with_cy(state.b, state.cc.cy, &mut state);
            (1, 4)
        }
        0x99 => {
            // SBB C
            sub_from_a_with_cy(state.c, state.cc.cy, &mut state);
            (1, 4)
        }
        0x9a => {
            // SBB D
            sub_from_a_with_cy(state.d, state.cc.cy, &mut state);
            (1, 4)
        }
        0x9b => {
            // SBB E
            sub_from_a_with_cy(state.e, state.cc.cy, &mut state);
            (1, 4)
        }
        0x9c => {
            // SBB H
            sub_from_a_with_cy(state.h, state.cc.cy, &mut state);
            (1, 4)
        }
        0x9d => {
            // SBB L
            sub_from_a_with_cy(state.l, state.cc.cy, &mut state);
            (1, 4)
        }
        0x9e => {
            // SBB M
            sub_from_a_with_cy(mem[state.at_hl()], state.cc.cy, &mut state);
            (1, 4)
        }
        0x9f => {
            // SBB A
            sub_from_a_with_cy(state.a, state.cc.cy, &mut state);
            (1, 4)
        }
        0xa0 => {
            // ANA B
            state.a &= state.b;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xa1 => {
            // ANA C
            state.a &= state.c;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xa2 => {
            // ANA D
            state.a &= state.d;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xa3 => {
            // ANA E
            state.a &= state.e;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xa4 => {
            // ANA H
            state.a &= state.h;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xa5 => {
            // ANA L
            state.a &= state.l;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xa6 => {
            // ANA M
            state.a &= mem[state.at_hl()];
            set_flags_clear_carry(state.a, &mut state);
            (1, 7)
        }
        0xa7 => {
            // ANA A
            state.a &= state.a;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xa8 => {
            // XRA B
            state.a ^= state.b;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xa9 => {
            // XRA C
            state.a ^= state.c;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xaa => {
            // XRA D
            state.a ^= state.d;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xab => {
            // XRA E
            state.a ^= state.e;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xac => {
            // XRA H
            state.a ^= state.h;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xad => {
            // XRA L
            state.a ^= state.l;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xae => {
            // XRA M
            state.a ^= mem[state.at_hl()];
            set_flags_clear_carry(state.a, &mut state);
            (1, 7)
        }
        0xaf => {
            // XRA A
            state.a ^= state.a;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xb0 => {
            // ORA B
            state.a |= state.b;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xb1 => {
            // ORA C
            state.a |= state.c;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xb2 => {
            // ORA D
            state.a |= state.d;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xb3 => {
            // ORA E
            state.a |= state.e;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xb4 => {
            // ORA H
            state.a |= state.h;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xb5 => {
            // ORA L
            state.a |= state.l;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xb6 => {
            // ORA M
            state.a |= mem[state.at_hl()];
            set_flags_clear_carry(state.a, &mut state);
            (1, 7)
        }
        0xb7 => {
            // ORA A
            state.a |= state.a;
            set_flags_clear_carry(state.a, &mut state);
            (1, 4)
        }
        0xb8 => {
            // CMP B
            compare(state.b, &mut state);
            (1, 4)
        }
        0xb9 => {
            // CMP C
            compare(state.c, &mut state);
            (1, 4)
        }
        0xba => {
            // CMP D
            compare(state.d, &mut state);
            (1, 4)
        }
        0xbb => {
            // CMP E
            compare(state.e, &mut state);
            (1, 4)
        }
        0xbc => {
            // CMP H
            compare(state.h, &mut state);
            (1, 4)
        }
        0xbd => {
            // CMP L
            compare(state.l, &mut state);
            (1, 4)
        }
        0xbe => {
            // CMP M
            compare(mem[state.at_hl()], &mut state);
            (1, 7)
        }
        0xbf => {
            // CMP A
            compare(state.a, &mut state);
            (1, 4)
        }
        0xc0 => {
            // RNZ
            if state.cc.z == 0 {
                state.stack_pop_pc(&mem);
                (0, 11)
            } else {
                (1, 5)
            }
        }
        0xc1 => {
            // POP B (pops BC pair)
            let value = state.stack_pop_bytes(&mem);
            state.set_bc(value);
            (1, 10)
        }
        0xc2 => {
            // JNZ addr
            if state.cc.z == 0 {
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 10)
            } else {
                (3, 10)
            }
        }
        0xc3 => {
            // JMP addr
            state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
            (0, 10)
        }
        0xc4 => {
            // CNZ word
            if state.cc.z == 0 {
                state.stack_push_pc(&mut mem, 3);
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 17)
            } else {
                (3, 11)
            }
        }
        0xc5 => {
            // PUSH B (pushes BC pair)
            state.stack_push_u8(&mut mem, state.b);
            state.stack_push_u8(&mut mem, state.c);
            (1, 11)
        }
        0xc6 => {
            // ADI byte
            add_to_a(mem[pc + 1], &mut state);
            (2, 7)
        }
        0xc7 => {
            // RST 0
            state.stack_push_pc(&mut mem, 1);
            state.set_pc(0);
            (0, 11)
        }
        0xc8 => {
            // RZ
            if state.cc.z == 1 {
                state.stack_pop_pc(&mem);
                (0, 11)
            } else {
                (1, 5)
            }
        }
        0xc9 => {
            // RET
            state.stack_pop_pc(&mem);
            (0, 10)
        }
        0xca => {
            // JZ word
            if state.cc.z == 1 {
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 10)
            } else {
                (3, 10)
            }
        }
        0xcc => {
            // CZ word
            if state.cc.z == 1 {
                state.stack_push_pc(&mut mem, 3);
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 17)
            } else {
                (3, 11)
            }
        }
        0xcd => {
            // CALL addr
            // save return addr to just after this op
            state.stack_push_pc(&mut mem, 3);
            // jump to the call site
            state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
            (0, 17)
        }
        0xce => {
            // ACI byte
            add_to_a_with_cy(mem[pc + 1], state.cc.cy, &mut state);
            (2, 7)
        }
        0xcf => {
            // RST 1
            state.stack_push_pc(&mut mem, 1);
            state.set_pc(0x0008);
            (0, 11)
        }
        0xd0 => {
            // RNC
            if state.cc.cy == 0 {
                state.stack_pop_pc(&mem);
                (0, 11)
            } else {
                (1, 5)
            }
        }
        0xd1 => {
            // POP D
            let value = state.stack_pop_bytes(&mem);
            state.set_de(value);
            (1, 10)
        }
        0xd2 => {
            // JNC word
            if state.cc.cy == 0 {
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 10)
            } else {
                (3, 10)
            }
        }
        0xd3 => {
            // OUT byte
            panic!("OUT shouldn't reach this"); // handled at a higher level
        }
        0xd4 => {
            // CNC word
            if state.cc.cy == 0 {
                state.stack_push_pc(&mut mem, 3);
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 17)
            } else {
                (3, 11)
            }
        }
        0xd5 => {
            // PUSH D (pushes DE pair)
            state.stack_push_u8(&mut mem, state.d);
            state.stack_push_u8(&mut mem, state.e);
            (1, 11)
        }
        0xd6 => {
            // SUI byte
            sub_from_a(mem[pc + 1], &mut state);
            (2, 7)
        }
        0xd7 => {
            // RST 2
            state.stack_push_pc(&mut mem, 1);
            state.set_pc(0x0010);
            (0, 11)
        }
        0xd8 => {
            // RC
            if state.cc.cy == 1 {
                state.stack_pop_pc(&mem);
                (0, 11)
            } else {
                (1, 5)
            }
        }
        0xda => {
            // JC word
            if state.cc.cy == 1 {
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 10)
            } else {
                (3, 10)
            }
        }
        0xdb => {
            // IN byte
            panic!("IN shouldn't reach this"); // handled at a higher level
        }
        0xdc => {
            // CC word
            if state.cc.cy == 1 {
                state.stack_push_pc(&mut mem, 3);
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 17)
            } else {
                (3, 11)
            }
        }
        0xde => {
            // SBI byte
            sub_from_a_with_cy(mem[pc + 1], state.cc.cy, &mut state);
            (2, 7)
        }
        0xdf => {
            // RST 3
            state.stack_push_pc(&mut mem, 1);
            state.set_pc(0x0018);
            (0, 11)
        }
        0xe0 => {
            // RPO
            if state.cc.p == 0 {
                state.stack_pop_pc(&mem);
                (0, 11)
            } else {
                (1, 5)
            }
        }
        0xe1 => {
            // POP H
            let value = state.stack_pop_bytes(&mem);
            state.set_hl(value);
            (1, 10)
        }
        0xe2 => {
            // JPO word
            if state.cc.p == 0 {
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 10)
            } else {
                (3, 10)
            }
        }
        0xe3 => {
            // XTHL
            // XTHL exchanges whatever is on the top of the stack with whatever is in the HL register pair.
            let mut stack_value = [0u8; 2];
            stack_value.clone_from_slice(&mem[state.sp..=state.sp + 1]);
            mem[state.sp] = state.l;
            mem[state.sp + 1] = state.h;
            state.set_hl(&stack_value);
            (1, 18)
        }
        0xe4 => {
            // CPO word
            if state.cc.p == 0 {
                state.stack_push_pc(&mut mem, 3);
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 17)
            } else {
                (3, 11)
            }
        }
        0xe5 => {
            // PUSH H (pushes HL pair)
            state.stack_push_u8(&mut mem, state.h);
            state.stack_push_u8(&mut mem, state.l);
            (1, 11)
        }
        0xe6 => {
            // ANI byte
            state.a &= mem[pc + 1];
            set_flags_clear_carry(state.a, &mut state);
            (2, 7)
        }
        0xe7 => {
            // RST 4
            state.stack_push_pc(&mut mem, 1);
            state.set_pc(0x0020);
            (0, 11)
        }
        0xe8 => {
            // RPE
            if state.cc.p == 1 {
                state.stack_pop_pc(&mem);
                (0, 11)
            } else {
                (1, 5)
            }
        }
        0xe9 => {
            // PCHL
            // The PCHL instruction will do a unconditional jump to the address in the HL register pair.
            state.set_pc(state.at_hl());
            (0, 5)
        }
        0xea => {
            // JPE addr
            if state.cc.p == 1 {
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 10)
            } else {
                (3, 10)
            }
        }
        0xeb => {
            // XCHG
            // XCHG exchanges HL with DE.
            let de_value = [state.e, state.d];
            state.d = state.h;
            state.e = state.l;
            state.set_hl(&de_value);
            (1, 4)
        }
        0xec => {
            // CPE addr
            if state.cc.p == 1 {
                state.stack_push_pc(&mut mem, 3);
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 17)
            } else {
                (3, 11)
            }
        }
        0xee => {
            // XRI byte
            state.a ^= mem[pc + 1];
            set_flags_clear_carry(state.a, &mut state);
            (2, 7)
        }
        0xef => {
            // RST 5
            state.stack_push_pc(&mut mem, 1);
            state.set_pc(0x0028);
            (0, 11)
        }
        0xf0 => {
            // RP
            if state.cc.s == 0 {
                state.stack_pop_pc(&mem);
                (0, 11)
            } else {
                (1, 5)
            }
        }
        0xf1 => {
            // POP PSW (pops the accumulator and flags)
            let psw = state.stack_pop_u8(&mut mem);
            state.a = state.stack_pop_u8(&mut mem);
            state.cc.z = set_flag_if!(psw & 0x01, 0x01);
            state.cc.s = set_flag_if!(psw & 0x02, 0x02);
            state.cc.p = set_flag_if!(psw & 0x04, 0x04);
            state.cc.cy = set_flag_if!(psw & 0x08, 0x08);
            state.cc.ac = set_flag_if!(psw & 0x10, 0x10);
            (1, 10)
        }
        0xf2 => {
            // JP addr
            if state.cc.s == 0 {
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 10)
            } else {
                (3, 10)
            }
        }
        0xf3 => {
            // DI
            state.int_enable = false;
            (1, 4)
        }
        0xf4 => {
            // CP addr
            if state.cc.s == 0 {
                state.stack_push_pc(&mut mem, 3);
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 17)
            } else {
                (3, 11)
            }
        }
        0xf5 => {
            // PUSH PSW (push the accumulator and flags)
            state.stack_push_u8(&mut mem, state.a);
            let psw = state.cc.z
                | state.cc.s << 1
                | state.cc.p << 2
                | state.cc.cy << 3
                | state.cc.ac << 4;
            state.stack_push_u8(&mut mem, psw);
            (1, 11)
        }
        0xf6 => {
            // ORI byte
            state.a |= mem[pc + 1];
            set_flags_clear_carry(state.a, &mut state);
            (2, 7)
        }
        0xf7 => {
            // RST 6
            state.stack_push_pc(&mut mem, 1);
            state.set_pc(0x0030);
            (0, 11)
        }
        0xf8 => {
            // RM
            if state.cc.s == 1 {
                state.stack_pop_pc(&mem);
                (0, 11)
            } else {
                (1, 5)
            }
        }
        0xf9 => {
            // SPHL
            // SPHL moves HL to SP (making the SP have a new address).
            state.sp = state.at_hl();
            (1, 5)
        }
        0xfa => {
            // JM word
            if state.cc.s == 1 {
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 10)
            } else {
                (3, 10)
            }
        }
        0xfb => {
            // EI
            state.int_enable = true;
            (1, 4)
        }
        0xfc => {
            // CM word
            if state.cc.s == 1 {
                state.stack_push_pc(&mut mem, 3);
                state.set_pc_bytes(&mem[pc + 1..=pc + 2]);
                (0, 17)
            } else {
                (3, 11)
            }
        }
        0xfe => {
            // CPI byte (compares immediate with A, only sets flags)
            compare(mem[pc + 1], &mut state);
            (2, 7)
        }
        0xff => {
            // RST 7
            state.stack_push_pc(&mut mem, 1);
            state.set_pc(0x0038);
            (0, 11)
        }
    };

    // adjust program counter based on instruction executed
    if pc_adj > 0 {
        state.set_pc(state.pc() + pc_adj);
    }

    // return the number of cycles spent
    cycles
}

pub fn generate_interrupt(interrupt: u16, mut mem: &mut [u8], state: &mut State) {
    if state.int_enable {
        state.stack_push_pc(&mut mem, 0);
        // set PC to low address (emulates RST)
        state.set_pc((interrupt * 8) as usize);
        // disable interrupts to avoid generating another one
        // expect the interrupt handler to call EI before RET
        state.int_enable = false;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn set_flag_if_macro() {
        let x = set_flag_if!(2 & 2, 2);
        assert_eq!(x, 1);
        let x = set_flag_if!(2 & 2, 0);
        assert_eq!(x, 0);
    }

    #[test]
    fn clear_flag_if_macro() {
        let x = clear_flag_if!(2 & 2, 2);
        assert_eq!(x, 0);
        let x = clear_flag_if!(2 & 2, 0);
        assert_eq!(x, 1);
    }

    #[test]
    fn parity_odd_bits_clear() {
        let x = 0b01110000;
        assert_eq!(parity(x), 0);
    }

    #[test]
    fn parity_even_bits_set() {
        let x = 0b01111000;
        assert_eq!(parity(x), 1);
    }

    #[test]
    fn daa_ok() {
        let mut state = State::new();
        state.a = 0x9b;
        daa(&mut state);
        assert_eq!(state.a, 1);
        assert_eq!(state.cc.cy, 1);
        assert_eq!(state.cc.ac, 1);
    }
}
