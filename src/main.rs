extern crate rodio;
extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use std::fs::File;
use std::io;
use std::io::prelude::*;

#[macro_use]
mod state;
mod emu;
mod machine;
mod sound;

use machine::*;
use state::State;

const MEMORY_SIZE: usize = 0xffff;
const INT_FRAME_LEN: u32 = 1000 / 120; // two frame interrupts are generated every 1/60th of a second
const FRAME_X_WIDTH: u32 = 256; // width of the space invaders frame buffer
const FRAME_Y_HEIGHT: u32 = 224; // height of the space invaders frame buffer
const CPU_CYCLES_PER_MS: u32 = 20_000; // number of CPU instruction cycles per ms (8080 CPU @ 2MHz)
const FRAME_INT_1: u16 = 1;
const FRAME_INT_2: u16 = 2;

/// Reads from `filename` and copies `memory_width` bytes into `memory` at `memory_start`.
fn load_file_to_memory(
    memory: &mut [u8],
    filename: &str,
    memory_start: usize,
    memory_width: usize,
) -> Result<(), io::Error> {
    let mut file = File::open(filename)?;
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer)?;
    if buffer.len() != memory_width {
        Err(io::Error::new(
            io::ErrorKind::Other,
            format!("File {} is not {} bytes long", filename, memory_width),
        ))
    } else {
        memory[memory_start..(memory_start + memory_width)].clone_from_slice(&buffer);
        Ok(())
    }
}

fn main() {
    // initialize SDL
    let sdl = sdl2::init().unwrap();
    let video = sdl.video().unwrap();
    let mut timer = sdl.timer().unwrap();
    let window = video
        .window(
            "Space Invaders",
            FRAME_Y_HEIGHT * 4 + 100,
            FRAME_X_WIDTH * 4,
        )
        .build()
        .unwrap();
    let mut canvas = window.into_canvas().present_vsync().build().unwrap();
    let mut event_pump = sdl.event_pump().unwrap();

    // initialize emulator
    let mut memory = [0; MEMORY_SIZE];
    let mut state = State::new();
    state.set_pc_bounds(machine::ROM_LOWER, machine::ROM_UPPER);
    state.sp = 0x2400; // work ram

    // load ROM
    load_file_to_memory(&mut memory, "invaders/invaders.h", 0, 0x800)
        .expect("Cannot load invaders.h");
    load_file_to_memory(&mut memory, "invaders/invaders.g", 0x800, 0x800)
        .expect("Cannot load invaders.g");
    load_file_to_memory(&mut memory, "invaders/invaders.f", 0x1000, 0x800)
        .expect("Cannot load invaders.f");
    load_file_to_memory(&mut memory, "invaders/invaders.e", 0x1800, 0x800)
        .expect("Cannot load invaders.e");

    // load sounds
    let mut sound_mgr = sound::SoundManager::new(10.0);
    sound_mgr
        .load_sound(SND_UFO, "invaders/sounds/0.wav")
        .expect("Cannot load 0.wav");
    sound_mgr
        .load_sound(SND_PLAYER_SHOT, "invaders/sounds/1.wav")
        .expect("Cannot load 1.wav");
    sound_mgr
        .load_sound(SND_PLAYER_DEATH, "invaders/sounds/2.wav")
        .expect("Cannot load 2.wav");
    sound_mgr
        .load_sound(SND_ENEMY_DEATH, "invaders/sounds/3.wav")
        .expect("Cannot load 3.wav");
    sound_mgr
        .load_sound(SND_MOVE_1, "invaders/sounds/4.wav")
        .expect("Cannot load 4.wav");
    sound_mgr
        .load_sound(SND_MOVE_2, "invaders/sounds/5.wav")
        .expect("Cannot load 5.wav");
    sound_mgr
        .load_sound(SND_MOVE_3, "invaders/sounds/6.wav")
        .expect("Cannot load 6.wav");
    sound_mgr
        .load_sound(SND_MOVE_4, "invaders/sounds/7.wav")
        .expect("Cannot load 7.wav");
    sound_mgr
        .load_sound(SND_UFO_DEATH, "invaders/sounds/8.wav")
        .expect("Cannot load 8.wav");

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    // create texture for video data
    let texture_creator = canvas.texture_creator();
    let mut texture = texture_creator
        .create_texture_streaming(
            sdl2::pixels::PixelFormatEnum::ARGB8888,
            FRAME_X_WIDTH,
            FRAME_Y_HEIGHT,
        )
        .unwrap();

    // emulate
    let mut last_interrupt_time = timer.ticks();
    let mut last_interrupt = FRAME_INT_1;
    let mut last_cpu_time = timer.ticks();
    'main: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => break 'main,
                Event::KeyDown {
                    keycode: Some(Keycode::Left),
                    ..
                } => machine::key_down(Inputs::Left, &mut state),
                Event::KeyUp {
                    keycode: Some(Keycode::Left),
                    ..
                } => machine::key_up(Inputs::Left, &mut state),
                Event::KeyDown {
                    keycode: Some(Keycode::Right),
                    ..
                } => machine::key_down(Inputs::Right, &mut state),
                Event::KeyUp {
                    keycode: Some(Keycode::Right),
                    ..
                } => machine::key_up(Inputs::Right, &mut state),
                Event::KeyDown {
                    keycode: Some(Keycode::Space),
                    ..
                } => machine::key_down(Inputs::Fire, &mut state),
                Event::KeyUp {
                    keycode: Some(Keycode::Space),
                    ..
                } => machine::key_up(Inputs::Fire, &mut state),
                Event::KeyDown {
                    keycode: Some(Keycode::C),
                    ..
                } => machine::key_down(Inputs::Coin, &mut state),
                Event::KeyUp {
                    keycode: Some(Keycode::C),
                    ..
                } => machine::key_up(Inputs::Coin, &mut state),
                Event::KeyDown {
                    keycode: Some(Keycode::Return),
                    ..
                } => machine::key_down(Inputs::Start, &mut state),
                Event::KeyUp {
                    keycode: Some(Keycode::Return),
                    ..
                } => machine::key_up(Inputs::Start, &mut state),
                _ => {}
            }
        }

        let cpu_cycles_needed = (timer.ticks() - last_cpu_time) * CPU_CYCLES_PER_MS;
        let mut cycles = 0;
        while cycles < cpu_cycles_needed {
            match memory[state.pc()] {
                0xd3 => {
                    // OUT port
                    match memory[state.pc() + 1] {
                        3 => {
                            state.hw.port3 = state.a;
                            machine::play_sounds(&sound_mgr, &mut state);
                        }
                        5 => {
                            state.hw.port5 = state.a;
                            machine::play_sounds(&sound_mgr, &mut state);
                        }
                        _ => machine::port_out(memory[state.pc() + 1], state.a, &mut state),
                    }
                    state.set_pc(state.pc() + 2);
                    cycles += 10;
                }
                0xdb => {
                    // IN port
                    state.a = machine::port_in(memory[state.pc() + 1], &state);
                    state.set_pc(state.pc() + 2);
                    cycles += 10;
                }
                _ => {
                    // everything else
                    cycles += emu::emulate_8080_op(&mut memory, &mut state);
                }
            }
        }
        last_cpu_time = timer.ticks();

        if timer.ticks() - last_interrupt_time > INT_FRAME_LEN {
            // generate video interrupt
            emu::generate_interrupt(last_interrupt, &mut memory, &mut state);
            if last_interrupt == FRAME_INT_1 {
                last_interrupt = FRAME_INT_2;
            } else {
                last_interrupt = FRAME_INT_1;
            }
            last_interrupt_time = timer.ticks();
        }

        machine::copy_frame_buffer(&memory[0x2400..=0x3fff], &mut texture);
        canvas
            .copy_ex(&texture, None, None, -90.0, None, false, false)
            .unwrap();
        canvas.present();
    }
}
