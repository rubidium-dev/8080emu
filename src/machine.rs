use crate::sound::SoundManager;
use crate::state::State;
use sdl2::render::Texture;

// Used for enforcing PC OOB checks.
pub const ROM_LOWER: usize = 0x0000;
pub const ROM_UPPER: usize = 0x1fff;

// Sounds
pub const SND_UFO: i32 = 0;
pub const SND_PLAYER_SHOT: i32 = 1;
pub const SND_PLAYER_DEATH: i32 = 2;
pub const SND_ENEMY_DEATH: i32 = 3;
pub const SND_MOVE_1: i32 = 4;
pub const SND_MOVE_2: i32 = 5;
pub const SND_MOVE_3: i32 = 6;
pub const SND_MOVE_4: i32 = 7;
pub const SND_UFO_DEATH: i32 = 8;

#[derive(Debug)]
pub enum Inputs {
    Left,
    Right,
    Coin,
    Fire,
    Start,
}

pub fn key_down(input: Inputs, state: &mut State) {
    // bit 0 = CREDIT (1 if deposit)
    // bit 1 = 2P start (1 if pressed)
    // bit 2 = 1P start (1 if pressed)
    // bit 3 = Always 1
    // bit 4 = 1P shot (1 if pressed)
    // bit 5 = 1P left (1 if pressed)
    // bit 6 = 1P right (1 if pressed)
    // bit 7 = Not connected
    state.hw.port1 |= match input {
        Inputs::Left => 0x20,  // set bit 5
        Inputs::Right => 0x40, // set bit 6
        Inputs::Coin => 0x01,  // set bit 0
        Inputs::Fire => 0x10,  // set bit 4
        Inputs::Start => 0x04, // set bit 2
    };
}

pub fn key_up(input: Inputs, state: &mut State) {
    state.hw.port1 &= match input {
        Inputs::Left => !0x20,  // clear bit 5
        Inputs::Right => !0x40, // clear bit 6
        Inputs::Coin => !0x01,  // clear bit 0
        Inputs::Fire => !0x10,  // clear bit 4
        Inputs::Start => !0x04, // clear bit 2
    };
}

pub fn port_in(port: u8, state: &State) -> u8 {
    match port {
        0 => {
            // bit 0 DIP4 (Seems to be self-test-request read at power up)
            // bit 1 Always 1
            // bit 2 Always 1
            // bit 3 Always 1
            // bit 4 Fire
            // bit 5 Left
            // bit 6 Right
            // bit 7 ? tied to demux port 7 ?
            0b00001110
        }
        1 => state.hw.port1,
        2 => {
            // bit 0 = DIP3 00 = 3 ships  10 = 5 ships
            // bit 1 = DIP5 01 = 4 ships  11 = 6 ships
            // bit 2 = Tilt
            // bit 3 = DIP6 0 = extra ship at 1500, 1 = extra ship at 1000
            // bit 4 = P2 shot (1 if pressed)
            // bit 5 = P2 left (1 if pressed)
            // bit 6 = P2 right (1 if pressed)
            // bit 7 = DIP7 Coin info displayed in demo screen 0=ON
            0b10000010
        }
        3 => {
            // Hardware bit shift, based on the values of the shift
            // registers (see OUT 4) and the shift offset (OUT 2).
            let value: u16 = ((state.hw.shift1 as u16) << 8) | state.hw.shift0 as u16;
            ((value >> (8 - state.hw.shift_offset)) & 0xff) as u8
        }
        _ => {
            println!("Undefined port read {}", port);
            0
        }
    }
}

pub fn port_out(port: u8, value: u8, mut state: &mut State) {
    match port {
        2 => {
            // Set hardware shift offset
            state.hw.shift_offset = value & 0x07;
        }
        3 => {
            panic!("Port 3 shouldn't happen here");
        }
        4 => {
            // Set hardware shift registers
            state.hw.shift0 = state.hw.shift1;
            state.hw.shift1 = value;
        }
        5 => {
            panic!("Port 5 shouldn't happen here");
        }
        6 => {} // watchdog, called a lot but no need to implement
        _ => println!("Undefined port write {}", port),
    }
}

fn chk_snd(port: u8, port_last: u8, bit: u8) -> bool {
    ((port & bit) == bit) && ((port_last & bit) == 0)
}

pub fn play_sounds(sound: &SoundManager, state: &mut State) {
    if state.hw.port3 != state.hw.port3_last {
        if chk_snd(state.hw.port3, state.hw.port3_last, 0x01) {
            // continuous UFO sound
            // TODO
        }
        if chk_snd(state.hw.port3, state.hw.port3_last, 0x02) {
            sound.play_sound(SND_PLAYER_SHOT);
        }
        if chk_snd(state.hw.port3, state.hw.port3_last, 0x04) {
            sound.play_sound(SND_PLAYER_DEATH);
        }
        if chk_snd(state.hw.port3, state.hw.port3_last, 0x08) {
            sound.play_sound(SND_ENEMY_DEATH);
        }
        state.hw.port3_last = state.hw.port3;
    }
    if state.hw.port5 != state.hw.port5_last {
        if chk_snd(state.hw.port5, state.hw.port5_last, 0x01) {
            sound.play_sound(SND_MOVE_1);
        }
        if chk_snd(state.hw.port5, state.hw.port5_last, 0x02) {
            sound.play_sound(SND_MOVE_2);
        }
        if chk_snd(state.hw.port5, state.hw.port5_last, 0x04) {
            sound.play_sound(SND_MOVE_3);
        }
        if chk_snd(state.hw.port5, state.hw.port5_last, 0x08) {
            sound.play_sound(SND_MOVE_4);
        }
        if chk_snd(state.hw.port5, state.hw.port5_last, 0x10) {
            sound.play_sound(SND_UFO_DEATH);
        }
        state.hw.port5_last = state.hw.port5;
    }
}

pub fn copy_frame_buffer(frame_buffer: &[u8], texture: &mut Texture) {
    // Translate the 1-bit space invaders frame buffer into
    // a 24bpp RGB texture.
    texture
        .with_lock(None, |buffer: &mut [u8], _pitch: usize| {
            for y in 0..224 {
                for x in (0..256).step_by(8) {
                    let pix = frame_buffer[(y * (256 / 8)) + (x / 8)];
                    for p in 0..8 {
                        let offset = (y * 256 * 4) + (x * 4) + (p * 4);
                        if (pix & (1 << p)) != 0 {
                            buffer[offset] = 0xff;
                            buffer[offset + 1] = 0xff;
                            buffer[offset + 2] = 0xff;
                            buffer[offset + 3] = 0xff;
                        } else {
                            buffer[offset] = 0;
                            buffer[offset + 1] = 0;
                            buffer[offset + 2] = 0;
                            buffer[offset + 3] = 0;
                        }
                    }
                }
            }
        })
        .unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn hardware_bit_shift() {
        let mut state = State::new();
        // Attempt a 3-bit shift of 0x1234
        // Represented as: 0001 0010 0011 0100
        //      retrieves:    ^ ^^^^ ^^^
        //             ie: 0x91
        port_out(2, 3, &mut state);
        port_out(4, 0x34, &mut state);
        port_out(4, 0x12, &mut state);
        assert_eq!(state.hw.shift_offset, 3);
        assert_eq!(state.hw.shift1, 0x12);
        assert_eq!(state.hw.shift0, 0x34);
        assert_eq!(port_in(3, &state), 0x91);
    }
}
