use crate::MEMORY_SIZE;

macro_rules! bytes_to_u16 {
    ( $high:expr, $low:expr ) => {
        (($high as u16) << 8) + $low as u16
    }
}

macro_rules! u16_to_bytes {
    ( $val:expr ) => {
        &[($val & 0xff) as u8, (($val >> 8) & 0xff) as u8]
    }
}

/// 8080 flags
pub struct ConditionCodes {
    /// Zero (Z), set if the result is zero.
    pub z: u8,
    /// Sign (S), set if the result is negative.
    pub s: u8,
    /// Parity (P), set if the number of 1 bits in the result is even.
    pub p: u8,
    /// Carry (CY), set if the last addition operation resulted in a carry or if the last subtraction operation required a borrow.
    pub cy: u8,
    /// Auxiliary carry (AC or H), used for binary-coded decimal arithmetic (BCD).
    pub ac: u8,
}

/// Machine-specific data
pub struct Hardware {
    /// Shift register 0 (used with ports 2, 3, 4).
    pub shift0: u8,
    /// Shift register 1 (used with ports 2, 3, 4).
    pub shift1: u8,
    /// Shift offset (used with ports 2, 3, 4).
    pub shift_offset: u8,
    /// Values for port 1 (controller input).
    pub port1: u8,
    /// Values for port 3 (analog sounds).
    pub port3: u8,
    /// Previous value for port3.
    pub port3_last: u8,
    /// Values for port 5 (analog sounds).
    pub port5: u8,
    /// Previous value for port 5.
    pub port5_last: u8,
}

pub struct State {
    /// Primary 8-bit accumulator register.
    pub a: u8,
    /// General purpose 8-bit register.
    pub b: u8,
    /// General purpose 8-bit register.
    pub c: u8,
    /// General purpose 8-bit register.
    pub d: u8,
    /// General purpose 8-bit register.
    pub e: u8,
    /// General purpose 8-bit register.
    pub h: u8,
    /// General purpose 8-bit register.
    pub l: u8,
    /// Stack pointer (16-bit memory address).
    pub sp: usize,
    /// Program counter (16-bit memory address).
    pc: usize,
    /// Lower bound of executable code (safety check).
    pc_lower: usize,
    /// Upper bound of executable code (safety check).
    pc_upper: usize,
    /// 8080 flags.
    pub cc: ConditionCodes,
    /// Controls whether hardware interrupts are enabled/disabled.
    pub int_enable: bool,
    /// Machine-specific state.
    pub hw: Hardware,
}

impl State {
    pub fn new() -> Self {
        State {
            a: 0,
            b: 0,
            c: 0,
            d: 0,
            e: 0,
            h: 0,
            l: 0,
            sp: 0,
            pc: 0,
            pc_lower: 0,
            pc_upper: 0xffff,
            cc: ConditionCodes {
                z: 0,
                s: 0,
                p: 0,
                cy: 0,
                ac: 0,
            },
            int_enable: true,
            hw: Hardware {
                shift0: 0,
                shift1: 0,
                shift_offset: 0,
                port1: 0b00001000, // bit 3 should always be 1
                port3: 0,
                port3_last: 0,
                port5: 0,
                port5_last: 0,
            }
        }
    }

    pub fn bc(&self) -> u16 {
        bytes_to_u16!(self.b, self.c)
    }

    pub fn set_bc(&mut self, bytes: &[u8]) {
        assert_eq!(bytes.len(), 2);
        self.c = bytes[0];
        self.b = bytes[1];
    }

    pub fn de(&self) -> u16 {
        bytes_to_u16!(self.d, self.e)
    }

    pub fn set_de(&mut self, bytes: &[u8]) {
        assert_eq!(bytes.len(), 2);
        self.e = bytes[0];
        self.d = bytes[1];
    }

    pub fn at_hl(&self) -> usize {
        bytes_to_u16!(self.h, self.l) as usize
    }

    pub fn hl(&self) -> u16 {
        bytes_to_u16!(self.h, self.l)
    }

    pub fn set_hl(&mut self, bytes: &[u8]) {
        assert_eq!(bytes.len(), 2);
        self.l = bytes[0];
        self.h = bytes[1];
    }

    pub fn pc(&self) -> usize {
        self.pc
    }

    pub fn set_pc_bounds(&mut self, lower: usize, upper: usize) {
        assert!(lower < upper);
        self.pc_lower = lower;
        self.pc_upper = upper;
    }

    pub fn set_pc(&mut self, value: usize) {
        assert!(value >= self.pc_lower && value <= self.pc_upper,
            format!("Something tried to execute outside ROM ({:04x}), from PC ({:04x})", value, self.pc));
        self.pc = value;
    }

    pub fn set_pc_bytes(&mut self, bytes: &[u8]) {
        assert_eq!(bytes.len(), 2);
        self.set_pc(bytes_to_u16!(bytes[1], bytes[0]) as usize);
    }

    pub fn set_sp(&mut self, bytes: &[u8]) {
        assert_eq!(bytes.len(), 2);
        self.sp = bytes_to_u16!(bytes[1], bytes[0]) as usize;
    }

    pub fn stack_pop_pc(&mut self, memory: &[u8]) {
        let new_pc = self.stack_pop_u16(&memory) as usize; 
        self.set_pc(new_pc);
    }

    pub fn stack_pop_u8(&mut self, memory: &[u8]) -> u8 {
        assert!(self.sp < MEMORY_SIZE);
        let value = memory[self.sp];
        self.sp += 1;
        value
    }

    pub fn stack_pop_u16(&mut self, memory: &[u8]) -> u16 {
        assert!(self.sp < MEMORY_SIZE - 1);
        let value = bytes_to_u16!(memory[self.sp + 1], memory[self.sp]);
        self.sp += 2;
        value
    }

    pub fn stack_pop_bytes<'a, 'b>(&'b mut self, memory: &'a [u8]) -> &'a [u8] {
        assert!(self.sp < MEMORY_SIZE - 1);
        let value = &memory[self.sp..=self.sp + 1];
        self.sp += 2;
        value
    }

    pub fn stack_push_pc(&mut self, mut memory: &mut [u8], offset: usize) {
        self.stack_push_u16(&mut memory, (self.pc + offset) as u16);
    }

    pub fn stack_push_u8(&mut self, memory: &mut [u8], value: u8) {
        assert!(self.sp > 0);
        memory[self.sp - 1] = value;
        self.sp -= 1;
    }

    pub fn stack_push_u16(&mut self, memory: &mut [u8], value: u16) {
        assert!(self.sp > 1);
        memory[self.sp - 2..=self.sp - 1].clone_from_slice(u16_to_bytes!(value));
        self.sp -= 2;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn bc_combined_register_get() {
        let mut state = State::new();
        state.b = 12;
        state.c = 34;
        assert_eq!(state.bc(), 3106);
    }

    #[test]
    fn bc_combined_register_set() {
        let mut state = State::new();
        state.set_bc(&[34, 12]);
        assert_eq!(state.b, 12);
        assert_eq!(state.c, 34);
    }

    #[test]
    fn de_combined_register_get() {
        let mut state = State::new();
        state.d = 12;
        state.e = 34;
        assert_eq!(state.de(), 3106);
    }

    #[test]
    fn de_combined_register_set() {
        let mut state = State::new();
        state.set_de(&[34, 12]);
        assert_eq!(state.d, 12);
        assert_eq!(state.e, 34);
    }

    #[test]
    fn hl_combined_register() {
        let mut state = State::new();
        state.h = 12;
        state.l = 34;
        assert_eq!(state.hl(), 3106);
    }

    #[test]
    fn hl_combined_register_set() {
        let mut state = State::new();
        state.set_hl(&[34, 12]);
        assert_eq!(state.h, 12);
        assert_eq!(state.l, 34);
    }

    #[test]
    fn at_hl_same_as_hl_combined() {
        let mut state = State::new();
        state.h = 12;
        state.l = 34;
        assert_eq!(state.hl() as usize, state.at_hl());
    }

    #[test]
    fn pc_set_ok() {
        let mut state = State::new();
        state.set_pc_bytes(&[34, 12]);
        assert_eq!(state.pc, 3106);
    }

    #[test]
    #[should_panic]
    fn pc_set_oob() {
        let mut state = State::new();
        state.set_pc_bounds(0, 0x1000);
        state.set_pc_bytes(&[44, 54]);
    }

    #[test]
    fn stack_pop_bytes_ok() {
        let mut state = State::new();
        let memory: [u8; 8] = [0, 0, 34, 12, 0, 0, 0, 0];
        state.sp = 2;
        let value = state.stack_pop_bytes(&memory);
        assert_eq!(state.sp, 4);
        assert_eq!([34, 12], value);
    }

    #[test]
    #[should_panic]
    fn stack_pop_bytes_underflow() {
        let mut state = State::new();
        let memory = [0u8; 8];
        state.sp = 8;
        state.stack_pop_bytes(&memory);
    }

    #[test]
    fn stack_pop_pc_ok() {
        let mut state = State::new();
        let memory: [u8; 8]= [0, 0, 34, 12, 0, 0, 0, 0];
        state.sp = 2;
        state.stack_pop_pc(&memory);
        assert_eq!(state.sp, 4);
        assert_eq!(state.pc, 3106);
    }

    #[test]
    #[should_panic]
    fn stack_pop_pc_underflow() {
        let mut state = State::new();
        let memory = [0u8; 8];
        state.sp = 7;
        state.stack_pop_pc(&memory);
    }

    #[test]
    fn stack_pop_u8_ok() {
        let mut state = State::new();
        let memory: [u8; 8] = [0, 0, 34, 12, 0, 0, 0, 0];
        state.sp = 2;
        let value = state.stack_pop_u8(&memory);
        assert_eq!(state.sp, 3);
        assert_eq!(value, 34);
        let value = state.stack_pop_u8(&memory);
        assert_eq!(state.sp, 4);
        assert_eq!(value, 12);
    }

    #[test]
    #[should_panic]
    fn stack_pop_u8_underflow() {
        let mut state = State::new();
        let memory = [0u8; 8];
        state.sp = 8;
        state.stack_pop_u8(&memory);
    }

    #[test]
    fn stack_pop_u16_ok() {
        let mut state = State::new();
        let memory: [u8; 8] = [0, 0, 34, 12, 0, 0, 0, 0];
        state.sp = 2;
        let value = state.stack_pop_u16(&memory);
        assert_eq!(state.sp, 4);
        assert_eq!(value, 3106);
    }

    #[test]
    #[should_panic]
    fn stack_pop_u16_underflow() {
        let mut state = State::new();
        let memory = [0u8; 8];
        state.sp = 8;
        state.stack_pop_u16(&memory);
    }

    #[test]
    fn stack_push_pc_ok() {
        let mut state = State::new();
        let mut memory = [0u8; 8];
        state.sp = 8;
        state.pc = 3106;
        state.stack_push_pc(&mut memory, 0);
        assert_eq!(state.sp, 6);
        assert_eq!(memory, [0, 0, 0, 0, 0, 0, 34, 12]);
        state.stack_push_pc(&mut memory, 0x80);
        assert_eq!(state.sp, 4);
        assert_eq!(memory, [0, 0, 0, 0, 162, 12, 34, 12]);
    }

    #[test]
    #[should_panic]
    fn stack_push_pc_out_of_mem() {
        let mut state = State::new();
        let mut memory = [0u8; 8];
        state.sp = 0;
        state.stack_push_pc(&mut memory, 0);
    }

    #[test]
    fn stack_push_u8_ok() {
        let mut state = State::new();
        let mut memory = [0u8; 8];
        state.sp = 8;
        state.stack_push_u8(&mut memory, 32);
        assert_eq!(state.sp, 7);
        assert_eq!(memory, [0, 0, 0, 0, 0, 0, 0, 32]);
        state.stack_push_u8(&mut memory, 9);
        assert_eq!(state.sp, 6);
        assert_eq!(memory, [0, 0, 0, 0, 0, 0, 9, 32]);
    }

    #[test]
    #[should_panic]
    fn stack_push_u8_out_of_mem() {
        let mut state = State::new();
        let mut memory = [0u8; 8];
        state.sp = 0;
        state.stack_push_u8(&mut memory, 32);
    }

    #[test]
    fn stack_push_u16_ok() {
        let mut state = State::new();
        let mut memory = [0u8; 8];
        state.sp = 8;
        state.stack_push_u16(&mut memory, 3106);
        assert_eq!(state.sp, 6);
        assert_eq!(memory, [0, 0, 0, 0, 0, 0, 34, 12]);
        state.stack_push_u16(&mut memory, 9999);
        assert_eq!(state.sp, 4);
        assert_eq!(memory, [0, 0, 0, 0, 15, 39, 34, 12]);
    }

    #[test]
    #[should_panic]
    fn stack_push_u16_out_of_mem() {
        let mut state = State::new();
        let mut memory = [0u8; 8];
        state.sp = 0;
        state.stack_push_u16(&mut memory, 3106);
    }

    #[test]
    fn stack_push_and_pop_pc() {
        let mut state = State::new();
        let mut memory = [0u8; 8];
        state.sp = 8;
        state.pc = 3106;
        state.stack_push_pc(&mut memory, 0x80);
        assert_eq!(state.sp, 6);
        assert_eq!(memory, [0, 0, 0, 0, 0, 0, 162, 12]);
        state.stack_pop_pc(&memory);
        assert_eq!(state.sp, 8);
        assert_eq!(state.pc, 3234);
    }

    #[test]
    fn stack_push_and_pop_u16() {
        let mut state = State::new();
        let mut memory = [0u8; 8];
        state.sp = 8;
        state.stack_push_u16(&mut memory, 3106);
        assert_eq!(state.sp, 6);
        assert_eq!(memory, [0, 0, 0, 0, 0, 0, 34, 12]);
        let value = state.stack_pop_u16(&memory);
        assert_eq!(state.sp, 8);
        assert_eq!(value, 3106);
    }
}
