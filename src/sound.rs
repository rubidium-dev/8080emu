use rodio::{Device, Source};
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::Cursor;

// Amusingly, sdl2-rust's audio implementation isn't threadsafe, so it's completely useless
// Rodio works for my purposes here.

pub struct SoundManager {
    amplify: f32,
    device: Device,
    sounds: HashMap<i32, Vec<u8>>,
}

impl SoundManager {
    pub fn new(amplify: f32) -> Self {
        SoundManager {
            amplify,
            device: rodio::default_output_device().expect("Unable to open audio device"),
            sounds: HashMap::new(),
        }
    }

    pub fn load_sound(&mut self, id: i32, wav: &str) -> Result<(), std::io::Error> {
        let mut file = File::open(wav)?;
        let mut buffer = Vec::new();
        file.read_to_end(&mut buffer)?;
        self.sounds.insert(id, buffer);
        Ok(())
    }

    pub fn play_sound(&self, id: i32) {
        let sound = self.sounds.get(&id);
        if sound.is_some() {
            let buffer = sound.unwrap().to_vec(); // copy
            let cursor = Cursor::new(buffer);
            let source = rodio::Decoder::new(cursor).expect("Unable to decode");
            rodio::play_raw(&self.device, source.convert_samples().amplify(self.amplify));
        }
    }
}
